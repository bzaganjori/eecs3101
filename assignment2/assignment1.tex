\documentclass{article}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage[margin=0.5in]{geometry}
\usepackage{titlesec}
\usepackage{listings}
\usepackage{courier}
\usepackage{amsmath}
\titleformat{\subsection}[runin]{}{}{}{}[]

\lstset{
	basicstyle=\ttfamily,
	keywordstyle=\bfseries,
	numbers=left,
	numberstyle=\small,
	numbersep=8pt,
	frame = single,
	framexleftmargin=15pt,
	showstringspaces=false,
	morekeywords={ALGORITHM, FUNCTION, INPUT, OUTPUT, PRE, POST, COND, if, else, end, return, then, for, range, do, while, function, elseif}
}

\begin{document}

\section*{Question 1}
Input : Array[1 .. n] of 0's and 1's
 \newline
Output : Length of longest monotone ascending sub-array 
\newline \newline
This problem will have a simple integer output. Since the array is not necessarily contiguous, individual elements from the array will be picked to produce the final ascending array. One solution would be to take all possible combinations of numbers and compare them to each other, giving us a total of $n!$ combinations - not a very attractive solution. We can complete this task in less time by using additional memory and develop an algorithm using the incremental method.
\newline
\newline
We will create two auxilliary arrays which will store a count of 0's and 1's at the specified index but which will initially be empty. The $Zeros$ array will be populated from index $1..n$ while the $Ones$ will be populated from $n..1$. For example, after both pass over the array [0,1,0,1,1,1,0,0], the resulting arrays for $Zeros$ and $Ones$ will be:
\newline
\newline
\centerline{Zeroes}
\centerline{$\rightarrow$[1,1,2,2,2,2,3,4] \space\space}
\centerline{[0,1,0,1,1,1,0,0]}
\centerline{ \space\space[4,4,3,3,2,1,0,0]$\leftarrow$}
\centerline{Ones}
\newline
\newline
Finally, we will traverse over the two new arrays, adding the two numbers at each index in search for a maximum number. The maximum will give us the length of the longest ascending subsequence within the input array.
\newline
Fact 1: Length of longest ascending monotonic subsequence will always be $<$ length of A.
\newline
Loop Invariant (9): Since the algorithm contains a simple for loop, the conditions at the start and end of the loop must always be true, $t \leq A.length + 1$. After each iteration we will update the counters in both arrays whether they have seen a 0 or a 1.
\newline
Loop Invariant (20): Will have the same loop invariant as the first loop since we are only traversing across an array. $t \leq A.length + 1$. This time, each iteration will check if there exists a new maximum length, if so we will update the current maximum.
\newline
\begin{lstlisting}
ALGORITHM: Longest monotone ascending subsequence 
PRE-COND: A[1..n] is an array of n 0/1 bits. 
POST-COND: output length of longest increasing subsequence of A[1..n] 
Zeros[A.length+1] := empty with Zeros[0] ;= 0
Ones[A.length+1] := empty with Ones[0] := 0
n := A.length 
max := 0

for t in range(1..n) do:
    if A[t] = 0 then
	Zero[t] := Zero[t-1]+1
    else
	Zero[t] := Zero[t-1]
    if A[n-t] = 1 then
	Ones[n-t] := Ones[n-t-1]+1
    else
	Ones[n-t] := Ones[n-t-1]
end for

for t in range(1..n) do:
    if Ones[t]+Zero[t] > max then
	max=Ones[t]+Zero[t];
end for

return max
end Longest monotone ascending subsequences
\end{lstlisting}
\newpage
\subsubsection*{Correctness: [Pre-Condition and PreLoop $\rightarrow$ Post Condition]}
Array must have only elements of 0's and 1's and we will get size of input array A.
\subsubsection*{[Loop-Inv. $\&$ not Exit-Cond. $\&$ Loop code $\rightarrow$ Loop-Inv.]}
The loop invariant will always be correct since the input array size is being used for the iteration and the size of two new arrays is equal. The exit condition will not be met until $t = n$, meaning we are still within the loop code. This loop invariant holds for the loop on line 20 as well. 
\newline
The algorithm takes $O(2n)$ time on the first loop and $O(n)$ time on the second loop. We get total of $O(2n) + O(n) = O(n)$ time complexity.




\section*{Question 2}
For this question, it is only necessary to show that a given binary tree has a saddle point and return that node. We are not concerned with where the node is or its value. The ancestors of that node need a value greater than the specified node while and all children must have a smaller value to be considered a saddle point. We will start by recursively checking left nodes first since the first comparision will occur when the current node has no children. First comparision will occur at the deepest left most node. We will need to keep track of the current minimum valued parent, this is the first piece of important information we must carry as we go deeper into the tree. Once we reach a node that fails as a saddle point, we begin carrying up the maximum value of that nodes children, which will either be a previously smaller value or the value of the current node if greater.

\begin{lstlisting}[mathescape=true]
ALGORITHM: SaddlePoint(T)
PRE-COND: T is a binary tree with each node having unique value
POST-COND: returns true when saddle point exists within T, false otherwise
    (x, nodeValue) $\leftarrow$ SP(T.root, $- \infty$)
    return nodeValue
end

function SP(N, parentMin)
PRE-COND: N is the root of a binary tree
POST-COND: Return tuple of a number and node. If saddle point found, output node value, 
           and exit recusive call. Else, minimum value of all child nodes with boolean 
           FALSE.
    if N = null then return NULL
    nodeMin := min(N.value, parentMin)
    
    (leftChild, node) = SP(N.left, nodeMin)
    if node != NULL then return (leftChild, node   # If found, start returning that value
    (rightChild, node) = SP(N.right, nodeMin)
    if node != NULL then return (rightChild, node) # If found, start returning that value
    
    childMax = max(leftChild, rightChild)
    if N.value $>$ childMax $\&$ N.value $<$ parentMin then:
	return (N.value, N) & EXIT
    return (max(childMax, N.value), NULL)
end
\end{lstlisting}
\subsubsection*{Correctness: [Pre-Condition \& Lines 11-21 $\rightarrow$ Post-Condition]}
Proof can be conducted using induction on size of tree at root of $T$. Base case is handled on line 13. Preconditions for left and right subtrees are covered in lines 16-19. By induction hypothesis, each recursive call will agree with pre-condition. $leftChild$ will contain the maximum value of all children in the left subtree while $rightChild$ will contain the maximum value of all children in the right subtree. Line 21 will check which child of the current node is greater, producing the second part of the post-condition. In line 22, it will check if the current node satisfies the first part of the post-condition, that node N is a saddle point. If we do find a saddle point, we will return the value of the saddle node, else we will return the maximum value of the maximum value of its children, as well as NULL. All return statements within the code will imply the post condition. Therefore, the code will return the correct output for any binary tree.
Time Complexity: This utilizes the post-order traversal algorithm, and will take $O(n)$ time, where $n$ is the number of nodes in $T$.






\section*{Question 3}
\subsubsection*{a.} Claim: $\forall n \geq 2$, a binary tree with $n$ nodes has a balanced edge if the removal of such edge results in two subtrees with at most $\left\lceil \dfrac{2n - 1}{3}\right\rceil$ nodes. ie. Induction is on the number of nodes in the tree.
\newline
Base case: Regardless of which side the second node is on, the edge between them proves to be a balanced edge.
$$ \left\lceil \frac{2(2) - 1}{3}\right\rceil = \left\lceil \frac{3}{3}\right\rceil = 1 $$
Inductive step: Let $L$ and $R$ denote the number of nodes in each left and right subtree respectively. This implies that $n = L + R + 1$. We will have two cases, one case will cover adding a node to $L$ and the other adding a node to $R$ but they will both be of the same format. Assume that $L$ and $R$ are two trees  that have $n$ less than maximum allowed nods.
$$L + 1 \leq \left\lceil \frac{2(n+1) - 1}{3}\right\rceil = \left\lceil \frac{2n+1}{3}\right\rceil$$
$$L \leq \left\lceil \frac{2n+1}{3} - 1\right\rceil = \left\lceil \frac{2n-2}{3}\right\rceil$$
$$L \leq \left\lceil \frac{2n-2}{3}\right\rceil \leq \left\lceil \frac{2n-1}{3}\right\rceil $$
$\therefore$ Induction hypothesis holds. The new subtree that has an additional node will now have a tighter bound on the maximum number of nodes since we need to ensure that the other side of the tree remains balanced. The same can be applied to $R$.
\subsubsection*{b.}
\begin{lstlisting}
INPUT: A binary tree T with N nodes
OUTPUT: Return balanced edge from T when found

ALGORITHM: bisection(T)
PRE-COND: T is a valid binary tree with more than 2 nodes.
POST-COND: output the node which is the child node of a balanced tree
	return findEdge(T.root, T.size)
end

FUNCTION whichNull(N1, N2):
PRE-COND: Both N1 and N2 are valid tree nodes
POST-COND: Return null if both are null, else return a node that is not null
    if N1 != NULL & N2 = NULL return N1
    elseif N1 = NULL & N2 != NULL return N2
    elseif N1 != NULL & N2 != NULL return N1
    else return NULL
end whichNull

FUNCTION findEdge(N, totalNodes):
PRE-COND: A valid node and the total number of nodes in a tree
POST-COND: Return a tuple (boolean, integer, node) that will describe the recursion thus 
far. Boolean will carry when a balanced tree node is found, integer will carry the 
number of subnodes of a node upon back recursion and node will carry the currently 
found balanced node since we are interested in any node.
	
    if N = null return (FALSE, 0, NULL)
	
    (foundEdgeL, curSubL, nodeL) = findEdge(N.left, totalNodes)
    (foundEdgeR, curSubR, nodeR) = findEdge(N.right, totalNodes)
	
    max := ceiling((2 * totalNodes - 1) / 3)
    subNodes := curSubL + curSubR + 1
    booleanVal := foundEdgeL | foundEdgeR
    newNode := whichNull(nodeL, nodeR)
	
    if subNodes < max & (totalNodes - subNodes) < max 
	return (TRUE, subNodes, N)
    else 
	return (booleanVal, subNodes, newNode)
end findEdge
\end{lstlisting}

\subsubsection*{Correctness: [Pre-Cond $\&$ lines 26-39 $\rightarrow$ Post-Cond]}
Can be proved by induction. In the base case, the post-condition is satisfied. We check whether the current node is valid (if null) or not. If it is, we return FALSE and 0 subchildren to the parent node. The post-condition for left and right recursive calls (line 28 and 29) is therefore correct since the nodes will always be valid. We will return a tuple of (boolean, integer, node) where the boolean value reflects if a balanced edge has been found, integer contains the number of subnodes at the current node and node carries the resulting node, if found. By induction hypothesis, since the post condition is correct, then the recursive calls will be correct. We then calculate the necessary parts of the code for comparison and back recursion. Line 31 will calculate the maximum allowed number of nodes for an edge to be considered balanced. Line 32 will compute the number of children at the current node. Line 33 will check the value of the returned boolean values for each subtree. If a node has been found to be balanced, the boolean value will change and therefore it will carry forward. Next, we check which node returned is valid to carry forward. We want to carry the non-null node if possible - one that is the root of a balanced edge. If both are non-null, it will not matter since we are interested in returning any balanced edge node. If both are null then we have not found one yet and carry on the recursive callback. Lastly, we return if a balanced tree root has been found or not. Both the pre and post conditions are strengthened.
The time complexity is that of post-order traversal, $O(n)$, the underlining algorithm.

\subsubsection*{c.}
The most evenly balanced partitions will occur when both partitions have the closest possible number of nodes to the upper bound. This only seems to occur, the most balanced subtree being equal to the upper bound, when the tree is a full binary tree and also when one subtree varies in heigh by 1 from the other. 

\subsubsection*{d.}
Removing $O(log n)$ edges implies that we will split the tree in halves. We check at the first node to see if the upper bound fails. If it fails, we recurse into the bigger tree. If not, we recurse into the tree with the smaller difference from the upper bound. Each time we must check which tree has the smaller difference from the upper bound. Once we find a balanced edge, we remove it and add the removed nodes to one of the sets. We continue this recursive call, alternating the adding of new trees to the sets untill we have removed O(log n) nodes. Since we are spliting the tree into two partitions each time, we will have two cases. if n is even and odd. If n was even, then both sets will have the equal number of nodes. Else, one set will have one node more, this is where A and B will take their ceilings and floors of the number of nodes they have in their sets.






\newpage
\section*{Question 4}
\subsubsection*{a.}
We are given the hint that this problem can be solved in $O(nlogn)$ time using a divide-conquer algorithm. We can look at algorithms we have previously learned that are considered divide-conquer and perform in $O(nlogn)$. It is easy to see that mergsort is the one. We will separate the list input in halves recursively, then we will compare them to each other, just like in mergesort. Just like in mergesort when two numbers are compared and merged, we will compare x-positions and heights. Merging them together will produce a new skyline (2 or more buildings). Now we begin merging skylines.

fact 1: input is given ordered from left to right
fact 2: the last building will have our upper bound on what to check for
fact 3: this is just a 2D array (width vs height) of buildings. A skyline is a bunch of "strips". Each array index will have a height.
\begin{lstlisting}
INPUT: List of triplets containing (LeftX, h, RightX) from left to right
OUTPUT: List of alternating x-coordinates and heights across the skyline, 
	(x1, h1, x2, h2, ..., xk, hk) where k is the number of height changes across 
	the skyline.

ALGORITHM: The Skyline Problem
	B := input array
	min := left x-coordinate of first building in B
	max := right x-coordinate of last building in B
	return output(mergesortSkyline(B, min, max))
end

FUNCTION mergesortSkyline(B[], min, max):
PRE-COND: Building list B and two integer values
POST-COND: Return sorted list consisting of elements from B
    if min = max:			# Base case - set height of index of same index
	skyline := list[2]
	skyline[0] := (B[min].left, B[min].height)
	skyline[1] := (B[min].right, 0)
	return skyline
    mid := (min + max) / 2
    skyline1 := mergeSkyline(B, min, mid)	# Recursive split
    skyline2 := mergeSkyline(B, mid + 1, max)	# Recursive split
    return mergeSkyline(skyline1, skyline2)	# Merge lists
end mergesortSkyline

FUNCTION mergeSkyline(s1, s2):
PRE-COND: s1 and s2 are lists of (x-coordinate, height) pairs
POST-COND: Return a new sorted list of (x-coordinate, height) pairs
    skyline := list[s1.size + s2.size]
	
    while s1 != empty & s2 != empty:	# Merge till one list is empty
	if s1.nextX < s2.nextX:
            skyline[s1.nextX] := (s1.nextX, max(s1.nextX.height, s2.nextX.height)
            remove(s1.nextX)
	else
            skyline[s2.nextX] := (s2.nextX, max(s1.nextX.height, s2.nextX.height)
            remove(s2.nextX)
		
	while s1 != empty:		# Merge the rest of s1
            skyline[nextX] := (s1.nextX, s1.nextX.height)
            remove(s1[nextX])
	while s2 != empty:		# Merge the rest of s2
            skyline[nextX] := (s2.nextX, s2.nextX.height)
            remove(s2.nextX)
	return skyline
end mergeSkyline

FUNCTION output(skyline):
    height := 0
    list := empty
    for t in range(1..skyline.length):
	if height != skyline[t].height
            height := skyline[t].height
            list.add(skyline[t].x)
            list.add(skyline[t].height)
	return list
end output
\end{lstlisting}
The important part of this problem is visualizing it as a 2D array of height vs. length. The x-coordinates of each building will provide the length. We begin with the divide-conquer part, line 16. We take the whole list of buildings and divide them using a variation of mergesort. With each sort call, we call the merge function. Except this time, when we stumble upon the smallest element, we check the height at each index and take the maximum since overlaping lines must be eliminated. Finally, the output function will be called to output the information of the skyline, (x-coordinate, height) pairs. This algorithm will work since we strengthen pre and post conditions of mergesort. Function on line 13 will divide the list so that it is easier to work on while line 27 will merge the various elements together into one sorted list (skyline). The running time of this algorithm is $O(nlogn)$.

\subsubsection*{b.}
The main problem with the skyline problem is that we do not know if two buildings are side-by-side or overlapping, and therefore we need to sort them in order to find out which ones are overlapping. This sorting is the most expensive portion of our algorithm. If a list was given sorted by the left x-coordinate, we could try developing a linear time algorithm. As I tried, but found out that it is not possible since we will not know how many buildings may be overlapping, there may be $n$ buildings but $n - 1$ overlap. This becomes very messy since we need to find which buildings right x-coordinate is first in line or which building is overlapping which building before the next building starts overlapping. We would need to keep a track of overlapping buildings, and even then, the best we can do is $O(nlogn)$

\end{document}